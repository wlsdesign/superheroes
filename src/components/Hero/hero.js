import React, { Component } from 'react';
import noImage from './img/no-image.png';
export default class Hero extends Component {
  constructor(){
    super();
    this.state = {
        iconHeart: 'fa-heart-o',
        heroesFavorites: [],
    }
}

  setFavorite = (id) => {
    let index = this.state.heroesFavorites.indexOf(id);
    if(this.state.heroesFavorites.includes(id)){
        this.state.heroesFavorites.splice(index, 1);
        this.setState({heroesFavorites: this.state.heroesFavorites});
    }else{
        this.setState({heroesFavorites: [...this.state.heroesFavorites, id]});
    }
}

  render() {
    const { id, url, name, race, handleInfoHero } = this.props;
    const { heroesFavorites } = this.state;
    return (
      <li className="list-heroes__item">
        <div className="list-heroes__item__box-image" onClick={ ()=> handleInfoHero(id)} style={{background: `url(${noImage}) center center no-repeat`, backgroundSize: 'cover'}}>
            <div className="list-heroes__item__image" style={{background: `url(${url}) center center no-repeat`, backgroundSize: 'cover'}}></div>
        </div>
        <div className="list-heroes__item__box-data">
            <div>
                <h2>{name}</h2>
                <span className="list-heroes__item--favorite" onClick={this.setFavorite.bind(this, id)}><i className={heroesFavorites.includes(id) ? 'fa fa-heart' : 'fa fa-heart-o'} aria-hidden="true"></i></span>
            </div>
            {race === 'null' ? (
                <h3><span>Raça: </span> <span>Indefinida</span></h3>
            ):(
                <h3><span>Raça: </span> <span>{race}</span></h3>
            )

            }
        </div>
    </li>
    );
  }
}
