import React, { Component } from 'react';
import './index.css';
import Hero from '../Hero/hero';

class Heroes extends Component {
    constructor(){
        super();
        this.state = {
            iconHeart: 'fa-heart-o',
            heroesFavorites: [],
        }
    }

    setFavorite = (id) => {
        let index = this.state.heroesFavorites.indexOf(id);
        if(this.state.heroesFavorites.includes(id)){
            this.state.heroesFavorites.splice(index, 1);
            this.setState({heroesFavorites: this.state.heroesFavorites});
        }else{
            this.setState({heroesFavorites: [...this.state.heroesFavorites, id]});
        }
    }
  
  render(){
    return (
        <ul className="list-heroes">
            {
            this.props.arrayMap.map(item => {
                return (
                    <Hero key={item.id} id={item.id} url={item.image.url} name={item.name} race={item.appearance.race} handleInfoHero={this.props.handleInfoHero} />
                   
                )
            })
            }
        </ul>
    );
  }
}

export default Heroes;