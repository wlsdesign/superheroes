import React, { Component } from 'react';
import Api from '../../services/api'; 
import './details.css';

class Hero extends Component {
    constructor(){
        super();
        this.state = {
            activeTab: '1',
            biography: {},
            appearance: {},
            power: {},
            work: {},
            connections: {},
            openMenu: ''
        }
    }

    handleClick = (event) => {
        event.preventDefault();
        this.setState({activeTab: event.target.id});
    }

    getBiography = async (atribute) => {
        const response = await Api.get(`${this.props.idHero}/${atribute}`);
        this.setState({biography: response.data});
    }
    getAppearance = async (atribute) => {
        const response = await Api.get(`${this.props.idHero}/${atribute}`)
        this.setState({appearance: response.data})
    }
    getPower = async (atribute) => {
        const response = await Api.get(`${this.props.idHero}/${atribute}`)
        this.setState({power: response.data})
    }
    getWork = async (atribute) => {
        const response = await Api.get(`${this.props.idHero}/${atribute}`)
        this.setState({work: response.data})
    }
    getConnections = async (atribute) => {
        const response = await Api.get(`${this.props.idHero}/${atribute}`)
        this.setState({connections: response.data})
    }

    openMenuMobile = () => {
        if(this.state.openMenu === ''){
            this.setState({openMenu: 'open'})
        }else{
            this.setState({openMenu: ''})
        }
        
    }

    componentDidMount(){
        this.getBiography('biography');
        this.getAppearance('appearance');
        this.getPower('powerstats');
        this.getWork('work');
        this.getConnections('connections');
    }

  render(){
    const { hero } = this.props;
    const { openMenu, biography, appearance, power, work, connections } = this.state;
    return (
        <div className="content__box-hero">
            <div className="content__box-hero__back">
                <span onClick={this.props.handleBack}><i className="fa fa-arrow-left" aria-hidden="true"></i> Voltar</span>
                <nav className="content__box-hero__menu">
                    <div id="menu-mobile" className={`jmenu-mobile ${openMenu}`} onClick={this.openMenuMobile}>
                        <span className="burguer"></span>
                        <span className="burguer"></span>
                        <span className="burguer"></span>
                    </div>
                    <ul className={`content__box-hero__menu__list ${openMenu}`}>
                        <li id="1" className={this.state.activeTab === '1' ? 'active' : ''} onClick={this.handleClick}>Biografia</li>
                        <li id="2" className={this.state.activeTab === '2' ? 'active' : ''} onClick={this.handleClick}>Aparência</li>
                        <li id="3" className={this.state.activeTab === '3' ? 'active' : ''} onClick={this.handleClick}>Poder</li>
                        <li id="4" className={this.state.activeTab === '4' ? 'active' : ''} onClick={this.handleClick}>Trabalho</li>
                        <li id="5" className={this.state.activeTab === '5' ? 'active' : ''} onClick={this.handleClick}>Conexões</li>
                    </ul>
                </nav>
            </div>
            
                    <div className="content__box-hero__parent" key={hero.id}>
                        <div className="content__box-hero__hero-data">
                            <div className="content__box-hero__hero-data--image" style={{background: `url(${hero.image.url}) center center no-repeat`, backgroundSize: 'cover'}}><span>{hero.name}</span></div>
                            
                            <div className="content__box-hero__hero-data__resume">
                                <div className={`content--tab ${this.state.activeTab === '1' ? 'active' : ''}`}>
                                    {
                                        
                                        <div key={biography.id}>
                                            <h2>Biografia</h2>
                                            <p>
                                                <label>Nome:</label>
                                                {Object.keys(biography).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(biography).length > 0 && <span>{biography.name == null ? '-' : `${biography.name}`}</span>}
                                            </p>
                                            <p>
                                                <label>Nome Completo:</label>
                                                {Object.keys(biography).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(biography).length > 0 && <span>{biography['full-name'] == null ? '-' : `${biography['full-name']}`}</span>}
                                            </p>
                                            <p>
                                                <label>Naturalidade:</label>
                                                {Object.keys(biography).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(biography).length > 0 && <span>{biography['place-of-birth'] == null ? '-' : `${biography['place-of-birth']}`}</span>}
                                            </p>
                                            <p>
                                                <label>Editora:</label>     
                                                {Object.keys(biography).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(biography).length > 0 && <span>{biography.publisher == null ? '-' : `${biography.publisher}`}</span>}
                                            </p>
                                            <p>
                                                <label>Primeira Aparição:</label>
                                                {Object.keys(biography).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(biography).length > 0 && <span>{biography['first-appearance'] == null ? '-' : `${biography['first-appearance']}`}</span>}
                                            </p>
                                        </div>
                                    }
                                </div>
                                <div className={`content--tab ${this.state.activeTab === '2' ? 'active' : ''}`}>
                                    <div key={appearance.id}>
                                        <h2>Aparência</h2>
                                        <p>
                                            <label>Raça:</label>
                                            {Object.keys(appearance).length === 0 && <span>Carregando...</span>}
                                            {Object.keys(appearance).length > 0 && <span>{appearance.race == null ? '-' : `${appearance.race}`}</span>}
                                        </p>
                                        <p>
                                            <label>Sexo:</label>
                                            {Object.keys(appearance).length === 0 && <span>Carregando...</span>}
                                            {Object.keys(appearance).length > 0 && <span>{appearance.gender == null ? '-' : `${appearance.gender}`}</span>}
                                        </p>
                                    </div>
                                </div>
                                 <div className={`content--tab ${this.state.activeTab === '3' ? 'active' : ''}`}>
                                    {
                                        <div key={power.id}>
                                            <h2>Estatísticas de poder</h2>
                                            <p>
                                                <label>Inteligência:</label>
                                                {Object.keys(power).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(power).length > 0 && <span>{power.intelligence == null ? '-' : `${power.intelligence}`}</span>}
                                            </p>
                                            <p>
                                                <label>Força:</label>
                                                {Object.keys(power).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(power).length > 0 && <span>{power.strength == null ? '-' : `${power.strength}`}</span>}
                                            </p>
                                            <p>
                                                <label>Poder:</label>
                                                {Object.keys(power).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(power).length > 0 && <span>{power.power == null ? '-' : `${power.power}`}</span>}
                                            </p>
                                            <p>
                                                <label>Combate:</label>
                                                {Object.keys(power).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(power).length > 0 && <span>{power.combat == null ? '-' : `${power.combat}`}</span>}
                                            </p>
                                            <p>
                                                <label>Velocidade:</label>
                                                {Object.keys(power).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(power).length > 0 && <span>{power.speed == null ? '-' : `${power.speed}`}</span>}
                                            </p>
                                        </div>
                                    }
                                </div>
                                <div className={`content--tab ${this.state.activeTab === '4' ? 'active' : ''}`}>
                                    {
                                        <div key={work.id}>
                                            <h2>Trabalho</h2>
                                            <p>
                                                <label>Ocupação:</label>
                                                {Object.keys(work).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(work).length > 0 && <span>{work.occupation == null ? '-' : `${work.occupation}`}</span>}
                                            </p>
                                            <p>
                                                <label>Base:</label>
                                                {Object.keys(work).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(work).length > 0 && <span>{work.base == null ? '-' : `${work.base}`}</span>}
                                            </p>
                                        </div>
                                    }
                                </div>
                                <div className={`content--tab ${this.state.activeTab === '5' ? 'active' : ''}`}>
                                    {
                                        <div key={connections.id}>
                                            <h2>Conexões</h2>
                                            <p>
                                                <label>Afiliados:</label>
                                                {Object.keys(power).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(power).length > 0 && <span>{connections['group-affiliation'] == null ? '-' : `${connections['group-affiliation']}`}</span>}
                                            </p>
                                            <p>
                                                <label>Parentes:</label>
                                                {Object.keys(power).length === 0 && <span>Carregando...</span>}
                                                {Object.keys(power).length > 0 && <span>{connections.relatives}</span>}
                                            </p>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                
      </div>
    );
  }
}

export default Hero;