import React from 'react';
import './footer.css';

function Footer() {
  return (
    <footer className="footer">
      <div className="wrap">
          <p className="footer__copyright">© Super-heróis 2019 <span>-</span> <span>Todos os direitos reservados</span></p>
      </div>
    </footer>
  );
}

export default Footer;